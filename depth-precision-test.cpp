#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <array>
#include <iostream>
#include <iomanip>
#include <thread>

static const int g_windowWidth = 1000;
static const int g_windowHeight = 600;

static const float g_badDepthValue = -0.00000001;

struct shader_data_t
{
    float data;
};

static const char* g_vertexShaderSource = R"_(
#version 430 core

layout (location = 0) in vec3 aPos;

layout(std430, binding = 1) buffer g_shader_data_vertex
{
    float cpuOutVertex;
};

void main()
{
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);

    cpuOutVertex = gl_Position.z;
}

)_";

static const char* g_fragmentShaderSource = R"_(
#version 430 core

out vec4 FragColor;
uniform vec4 ourColor;

layout(std430, binding = 2) buffer g_shader_data_fragment
{
    float cpuOutFragment;
};

void main()
{
    FragColor = ourColor;

    cpuOutFragment = gl_FragCoord.z;
}

)_";  

GLFWwindow* CreateWindowAndOpengContext(int width, int height)
{
    glfwInit();
    glfwSetErrorCallback([](int, const char *err_str)
                         { std::cout << "GLFW Error: " << err_str << std::endl; });

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_DEPTH_BITS, 24);
	glfwWindowHint(GLFW_STENCIL_BITS, 0);

    GLFWwindow* window = glfwCreateWindow(width, height, "Depth precision test", NULL, NULL);

    if (window == NULL)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        std::terminate();
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << "Failed to initialize GLAD" << std::endl;
        std::terminate();
    }

    return window;
}

unsigned int CompileShaders(const char* vertexShaderSource, const char* fragmentShaderSource)
{
    unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    int  success = 0;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

    if(!success)
    {
        char infoLog[512] = {0};
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        std::terminate();
    }

    unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);

    if(!success)
    {
        char infoLog[512] = {0};
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
        std::terminate();
    }

    unsigned int shaderProgram = 0;
    shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);

    if(!success) {
        char infoLog[512] = {0};
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
        std::terminate();
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return shaderProgram;
}

unsigned int CreateFramebuffer(GLenum depthType, int width, int height)
{
    unsigned int fbo = 0;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    unsigned int texture = 0;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);  

    unsigned int rbo = 0;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, depthType, width, height);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cerr << "Not complete framebuffer!\n";
        std::terminate();
    }

    return fbo;
}

GLuint CreateSSBOFragment()
{
    GLuint ssbo = 0;

    glGenBuffers(1, &ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(shader_data_t), NULL, GL_DYNAMIC_COPY);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, ssbo);

    return ssbo;
}

GLuint CreateSSBOVertex()
{
    GLuint ssbo = 0;

    glGenBuffers(1, &ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(shader_data_t), NULL, GL_DYNAMIC_COPY);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ssbo);

    return ssbo;
}

float GetGpuDataOut(GLuint ssbo)
{
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);

    GLvoid* p = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    if(0 == p)
    {
        std::cerr << "0 == p\n";
        std::terminate();
    }

    shader_data_t shader_data = {0.0f};
    memcpy(&shader_data, p, sizeof(shader_data_t));
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    return shader_data.data;
}

void DrawRectangle(std::array<float, 12> vertices, std::array<unsigned int, 6> indices)
{
    static unsigned int VAO = 0;
    static unsigned int VBO = 0;
    static unsigned int EBO = 0;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices.data(), GL_STATIC_DRAW); 

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

int main()
{
    GLFWwindow* window = CreateWindowAndOpengContext(g_windowWidth, g_windowHeight);

    int width = 0, height = 0;
    glfwGetFramebufferSize(window, &width, &height);

    glViewport(0, 0, width, height);

    unsigned int fbo = CreateFramebuffer(GL_DEPTH_COMPONENT16, width, height);

    unsigned int shaderProgram = CompileShaders(g_vertexShaderSource, g_fragmentShaderSource);
    glUseProgram(shaderProgram);

    GLuint ssboVertex = CreateSSBOVertex();
    GLuint ssboFragment = CreateSSBOFragment();

    // Settings from renderdoc

    glFrontFace(GL_CW);  
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    // glDepthRange(0.0, 0.9);

    // Draw 2 rectangles
//while(!glfwWindowShouldClose(window)) {
    
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUniform4f(glGetUniformLocation(shaderProgram, "ourColor"), 0.0f, 0.0f, 0.0f, 1.0f);
    
    DrawRectangle(
        { // Vertices
            -1.0f,  -1.0f, 0.0f, // Bottom left
            1.0f,  -1.0f,  0.0f, // Bottom right
            -1.0f,  +1.0f, 0.0f, // Top left
            1.0f,  1.0f,   0.0f // Top right
        }, 
        { // Indices
            0, 3, 1,
            3, 0, 2
        }
    );

    {
        std::cout << "1 - gl_Position.z = " << std::defaultfloat << 0.0f << " | " << std::hexfloat << 0.0f << std::endl;

        unsigned short x = 0;
        glReadPixels( 0, 0, 1, 1, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, &x );
        float vertexData = GetGpuDataOut(ssboVertex);
        float fragmentData = GetGpuDataOut(ssboFragment);
        std::cerr << ">>>  Vertex gl_Position.z SSBO:    " << 
            std::defaultfloat << vertexData << std::hexfloat << " | " << vertexData << std::endl;

        std::cerr << ">>>  Fragment gl_FragCoord.z SSBO: " <<
             std::defaultfloat << fragmentData << std::hexfloat << " | " << fragmentData << std::endl;
        std::cerr << ">>>  Resulting depth internal 16bit uint value = 0x" << std::hex << x << std::endl;
        std::cerr << std::endl;
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    {
        std::cout << "2 - Depth buffer 16bit integer value after clear: 0x";
        unsigned short x = 0;
        glReadPixels( 0, 0, 1, 1, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, &x );
        std::cout << std::hex << x << std::endl;
        std::cerr << std::endl;
    }

    glUniform4f(glGetUniformLocation(shaderProgram, "ourColor"), 1.0f, 0.0f, 0.0f, 1.0f);
    
    DrawRectangle(
        { // Vertices
            -1.0f,  -1.0f, g_badDepthValue, // Bottom left
            1.0f,  -1.0f,  g_badDepthValue, // Bottom right
            -1.0f,  +1.0f, g_badDepthValue, // Top left
            1.0f,  1.0f,   g_badDepthValue // Top right
        }, 
        { // Indices
            0, 3, 1,
            3, 0, 2
        }
    );

    {
        std::cout << "3 - gl_Position.z = " << std::defaultfloat << g_badDepthValue << " | " << std::hexfloat << g_badDepthValue << std::endl;

        unsigned short x = 0;
        glReadPixels( 0, 0, 1, 1, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, &x );
        float vertexData = GetGpuDataOut(ssboVertex);
        float fragmentData = GetGpuDataOut(ssboFragment);
        std::cerr << ">>>  Vertex gl_Position.z SSBO:    " << 
            std::defaultfloat << vertexData << std::hexfloat << " | " << vertexData << std::endl;

        std::cerr << ">>>  Fragment gl_FragCoord.z SSBO: " <<
             std::defaultfloat << std::setprecision(10) << fragmentData << std::hexfloat << " | " << fragmentData << std::endl;
        std::cerr << ">>>  Resulting depth internal 16bit uint value = 0x" << std::hex << x << std::endl;
        std::cerr << std::endl;
    }
        
    glUniform4f(glGetUniformLocation(shaderProgram, "ourColor"), 0.0f, 1.0f, 0.0f, 1.0f);
    
    DrawRectangle(
        { // Vertices
            -1.0f/2,  -1.0f/2, 0.0f, // Bottom left
            1.0f/2,  -1.0f/2,  0.0f, // Bottom right
            -1.0f/2,  +1.0f/2, 0.0f, // Top left
            1.0f/2,  1.0f/2,   0.0f // Top right
        }, 
        { // Indices
            0, 3, 1,
            3, 0, 2
        }
    );

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
    glBlitFramebuffer(0, 0, g_windowWidth, g_windowHeight,
			          0, 0, g_windowWidth, g_windowHeight,
			          GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glfwSwapBuffers(window);

    while(!glfwWindowShouldClose(window)) {
        std::this_thread::yield();
        glfwPollEvents();
    }
}
