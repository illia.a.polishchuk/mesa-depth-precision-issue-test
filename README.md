# Mesa depth precision issue test
#### Test program for reproducing depth test precision difference for INTEL MESA and other gpus
## Building
```bash
mkdir build
cd build
cmake ..
cmake --build .
```
### Precondition:
- GL_DEPTH_TEST enabled<br /> 
- GL_LEQUAL depth func enabled<br /> 

- Rectangle with Z coord equal to **-0.00000001** drawed<br /> 
### Test:
Draw rectangle on top of previous with Z coord equal to 0.0
### Intel linux mesa IRIS behavior:
Depth test failed.<br />
```bash
./depth-precision-test
1 - gl_Position.z = 0 | 0x0p+0
>>>  Vertex gl_Position.z SSBO:    0 | 0x0p+0
>>>  Fragment gl_FragCoord.z SSBO: 0.5 | 0x1p-1
>>>  Resulting depth internal 16bit uint value = 0x8000

2 - Depth buffer 16bit integer value after clear: 0xffff

3 - gl_Position.z = -1e-08 | -0x1.5798eep-27
>>>  Vertex gl_Position.z SSBO:    -1e-08 | -0x1.5798eep-27
>>>  Fragment gl_FragCoord.z SSBO: 0.4999999702 | 0x1.fffffep-2
>>>  Resulting depth internal 16bit uint value = 0x7fff
```
Result:<br /> 
![test_intel_iris_mesa_linux.png](./img/test_intel_iris_mesa_linux.png)
### LLVMpipe linux mesa behavior:
Depth test passed.<br /> 
```bash
1 - gl_Position.z = 0 | 0x0p+0
>>>  Vertex gl_Position.z SSBO:    0 | 0x0p+0
>>>  Fragment gl_FragCoord.z SSBO: 0.5 | 0x1p-1
>>>  Resulting depth internal 16bit uint value = 0x8000

2 - Depth buffer 16bit integer value after clear: 0xffff

3 - gl_Position.z = -1e-08 | -0x1.5798eep-27
>>>  Vertex gl_Position.z SSBO:    -1e-08 | -0x1.5798eep-27
>>>  Fragment gl_FragCoord.z SSBO: 0.5 | 0x1p-1
>>>  Resulting depth internal 16bit uint value = 0x8000
```
![image-1.png](./img/test_llvmpipe_mesa_linux.png)
### NVidia Windows behavior
Depth test passed.<br />
```bash
./depth-precision-test
1 - gl_Position.z = 0 | 0x0.0000000000000p+0
>>>  Vertex gl_Position.z SSBO:    0 | 0x0.0000000000000p+0
>>>  Fragment gl_FragCoord.z SSBO: 0.5 | 0x1.0000000000000p-1
>>>  Resulting depth internal 16bit uint value = 0x7fff

2 - Depth buffer 16bit integer value after clear: 0xffff

3 - gl_Position.z = -1e-08 | -0x1.5798ee0000000p-27
>>>  Vertex gl_Position.z SSBO:    -1e-08 | -0x1.5798ee0000000p-27
>>>  Fragment gl_FragCoord.z SSBO: 0.5 | 0x1.0000000000000p-1
>>>  Resulting depth internal 16bit uint value = 0x7fff
```
![image.png](./img/test_nvidia_windows.png)
